@file:Suppress("OPT_IN_USAGE")

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose")
//    kotlin("plugin.serialization")
}


val ktorVersion = extra["ktor.version"]
val coroutinesVersion = extra["coroutines.version"]

kotlin {
    jvm("desktop")
    js(IR) {
        browser()
    }
    wasm {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":domain"))
                implementation(compose.ui)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(compose.runtime)
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.0-Beta")

//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
//                implementation("io.ktor:ktor-client-core:$ktorVersion")
            }
        }


    }
}

