package net.moindron.multi.common.ui

import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import net.moindron.multi.domain.data.Client

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun mainapp() {
    val coroutineScope = rememberCoroutineScope()
    val applicationManager = remember { ApplicationManager(coroutineScope) }

    val applicationState by applicationManager.applicationState.collectAsState()

    // Remember our own LazyListState
    val listState = rememberLazyListState()


    Row {
        Column {
            Text("utilisateur :")
            LazyColumn (state = listState){
                stickyHeader {
                    ClientHeader()
                }


                items(items=applicationState.clients,
                key={it.id}
                ) { client ->
                    ClientRow(client)
                }
            }
        }


        Button(
            onClick = { applicationManager.loadClients() },
            modifier = Modifier.padding(vertical = 10.dp)
        ) {
            Text("chercher")
        }
    }
}
@Composable
fun LazyItemScope.ClientHeader() {
    Row{
        Text("nom")
        Spacer(Modifier.width(10.dp))
        Text("prenon")
    }
}

@Composable
fun LazyItemScope.ClientRow(client: Client) {
    Row{
        Text(client.name)
        Spacer(Modifier.width(10.dp))
        Text(client.firstName)
    }
}
