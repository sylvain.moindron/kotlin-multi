package net.moindron.multi.common.ui

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import net.moindron.multi.domain.ClientServiceImpl
import net.moindron.multi.domain.data.Client
import net.moindron.multi.domain.data.ClientFilter
import net.moindron.multi.domain.port.ClientRepository
import kotlin.random.Random


data class ApplicationState(
    val clients: List<Client> = emptyList()
)

class ApplicationManager(
    val coroutineScope: CoroutineScope
) {
    private val clientService = ClientServiceImpl(PipoclientRepository)

    private val _applicationState = MutableStateFlow(ApplicationState())
    val applicationState = _applicationState.asStateFlow()

    private var fetchJob: Job? = null

    fun loadClients() {
        if (fetchJob?.isActive == true) {
            fetchJob?.cancel()
        }
        _applicationState.update {
            it.copy(clients = emptyList())
        }
        fetchJob = coroutineScope.launch {
            clientService.findAll()
                .collectIndexed {index, client ->
                    println("recuperation $index")
                    _applicationState.update {
                        it.copy(clients = it.clients + client)
                    }
                }

        }


    }


}

object PipoclientRepository : ClientRepository {

    override fun findAll(filter: ClientFilter): Flow<Client> {
        return flow {
            for (cpt in 1..100) {
                emit(Client("id$cpt", "pipo$cpt", "zorglub$cpt", null, Random.nextInt(0, 100)))
               // delay(10)
            }
        }
    }

}
