@file:Suppress("OPT_IN_USAGE")
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackConfig
plugins {
    id("org.jetbrains.compose")
    kotlin("multiplatform")
}



val coroutinesVersion = extra["coroutines.version"]

kotlin {
    js(IR) {
        moduleName = "connectFour"
        browser {
            commonWebpackConfig {
                devServer = (devServer ?: KotlinWebpackConfig.DevServer()).copy(
                    open = mapOf(
                        "app" to mapOf(
                            "name" to "chromium",
                        )
                    ),
                )
            }
        }
        binaries.executable()
    }
    wasm {
        moduleName = "connectFour"
        browser {
            commonWebpackConfig {
                devServer = (devServer ?: KotlinWebpackConfig.DevServer()).copy(
                    open = mapOf(
                        "app" to mapOf(
                            "name" to "chromium",
                        )
                    ),
                )
            }
        }
        binaries.executable()
    }

    sourceSets {

        val jsWasmMain by creating {
            dependencies {
                implementation(project(":client:common"))
                implementation(compose.runtime)
                implementation(compose.ui)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
                @OptIn(org.jetbrains.compose.ExperimentalComposeLibrary::class)
                implementation(compose.components.resources)
            }
        }
        val jsMain by getting {
            dependsOn(jsWasmMain)
        }
        val wasmMain by getting {
            dependsOn(jsWasmMain)
        }

    }
}
compose.experimental {
    web.application {}
}