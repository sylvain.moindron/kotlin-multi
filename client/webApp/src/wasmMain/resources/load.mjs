import {instantiate} from './connectFour.uninstantiated.mjs';

await wasmSetup;
instantiate({skia: Module['asm']});