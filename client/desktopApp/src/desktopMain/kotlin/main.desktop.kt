import androidx.compose.material.MaterialTheme
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import net.moindron.multi.common.ui.mainapp


fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Client App",
        state = WindowState(),
        ) {
        MaterialTheme {
            mainapp()
        }

    }

}


