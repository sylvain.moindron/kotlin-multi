package net.moindron.multi.domain.data

data class Client(
    val id: String,
    val firstName: String,
    val name: String,
    val address: Address?,
    val age: Int,
) {
    val isAdult by lazy { age > 18 }
}

data class Address(
    val street: String?,
    val zipCode: String?,
    val city: String?,
)

data class ClientFilter(
    val id: String? = null,
    val fistName: String? = null,
    val name: String? = null,
)