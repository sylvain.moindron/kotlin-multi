package net.moindron.multi.domain.port

import kotlinx.coroutines.flow.Flow
import net.moindron.multi.domain.data.Client
import net.moindron.multi.domain.data.ClientFilter


interface ClientService {
    fun findAll(filter: ClientFilter = ClientFilter()): Flow<Client>

}

interface ClientRepository {
    fun findAll(filter: ClientFilter): Flow<Client>
}