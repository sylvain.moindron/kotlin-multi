package net.moindron.multi.domain


import kotlinx.coroutines.flow.Flow
import net.moindron.multi.domain.Exeption.DomainException
import net.moindron.multi.domain.data.Client
import net.moindron.multi.domain.data.ClientFilter
import net.moindron.multi.domain.port.ClientRepository
import net.moindron.multi.domain.port.ClientService

class ClientServiceImpl(
    private val clientRepository: ClientRepository
) : ClientService {
    override fun findAll(filter: ClientFilter): Flow<Client> {
        try {
            return clientRepository.findAll(filter)
        }catch (e : Exception) {
            throw DomainException(e)
        }
    }
}