@file:Suppress("OPT_IN_USAGE")

plugins {
    kotlin("multiplatform")
}

val coroutinesVersion = extra["coroutines.version"]

kotlin {
    jvmToolchain(17)
    jvm()
    js(IR) {
        browser()
    }
    wasm {
        browser()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.0-Beta")
            }

        }
    }
}