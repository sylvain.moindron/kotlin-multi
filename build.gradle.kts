plugins {
    id("org.jetbrains.compose") apply false
    kotlin("multiplatform") apply false
//    kotlin("plugin.serialization") version "1.8.20"
    kotlin("plugin.spring") version "1.8.20" apply false
    kotlin("plugin.jpa") version "1.8.20" apply false

}

group = "net.moindron"
version = "1.0-SNAPSHOT"
allprojects {
    repositories {
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven("https://maven.pkg.jetbrains.space/kotlin/p/wasm/experimental")
        maven ("https://maven.pkg.jetbrains.space/public/p/ktor/eap")
        mavenCentral()

    }
}
