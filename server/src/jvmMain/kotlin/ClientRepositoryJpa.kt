package net.moindron.multi.server

import org.springframework.data.annotation.Id
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import net.moindron.multi.domain.data.Address
import net.moindron.multi.domain.data.Client
import net.moindron.multi.domain.data.ClientFilter
import net.moindron.multi.domain.port.ClientRepository
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.util.*

@Service
class ClientRepositoryJpa(
    private val clientDao: ClientDao
) : ClientRepository {
    override fun findAll(filter: ClientFilter): Flow<Client> {
        return clientDao.findAll().map(ClientEntity::toDomain)
    }
}


@Repository
interface ClientDao : CoroutineCrudRepository<ClientEntity, UUID>

@Table("clients")
data class ClientEntity(
    @field:Id
    val id: UUID? = null,
    val firstName: String,
    val name: String,
    val age: Int,
    val street: String?,
    val zipCode: String?,
    val city: String?,
) {

    fun toDomain() = Client(
        id = id.toString(),
        firstName = firstName,
        name = name,
        address = Address(
            street = street,
            zipCode = zipCode,
            city = city,
        ),
        age = age,
    )

}