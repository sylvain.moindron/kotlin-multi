package net.moindron.multi.server

import net.moindron.multi.domain.ClientServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Configuration {

    @Bean
    fun clientService(clientRepositoryJpa: ClientRepositoryJpa) = ClientServiceImpl(clientRepositoryJpa)





}
