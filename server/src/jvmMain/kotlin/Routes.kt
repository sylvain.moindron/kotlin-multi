package net.moindron.multi.server

import net.moindron.multi.domain.port.ClientService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.coRouter

@Configuration
class Routes {

    @Bean
    fun clientRouter(clientService: ClientService) = coRouter {
        "/api".nest {
            accept(APPLICATION_JSON).nest {
                GET("/clients") {req->
                    ok().bodyAndAwait(clientService.findAll())
                }
            }
        }
    }
}