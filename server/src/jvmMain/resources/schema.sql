create table if not exists clients (
    id uuid default random_uuid() primary key,
    first_name varchar(100) not null,
     name varchar(100) not null,
     age bigint not null,
     street varchar(100),
     zip_code varchar(100),
     city varchar(100)
);