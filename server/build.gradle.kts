
plugins {
    id("org.springframework.boot") version "3.0.6"
    id("io.spring.dependency-management") version "1.1.0"
    kotlin("multiplatform")
    kotlin("plugin.spring")
    kotlin("plugin.jpa")

}

val coroutinesVersion = extra["coroutines.version"]

kotlin {
    jvmToolchain(17)
    jvm{
        withJava()
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(project(":domain"))
                implementation("org.springframework.data:spring-data-r2dbc")
                implementation("org.springframework.boot:spring-boot-starter-webflux")
                implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
                implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
                implementation("org.jetbrains.kotlin:kotlin-reflect")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.0-Beta")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
                runtimeOnly("io.r2dbc:r2dbc-h2")

            }

        }
    }
}